Pb Downloadform
=================

This extension provides a simple plugin which displays a form which must be filled before a user can download a file. The files can be selected via FAL.
After the user filled the form and downloads the file, a database record containing the user input will be created. Also an email will be send
to the given e-mail address. There are several configuration parameters to control the behaviour of the plugin. 

## Features

* Create a simple download form to let users download a file from a list of FAL files
* Includes the [Magnific-Popup](https://github.com/dimsemenov/Magnific-Popup) library 


## Installation

### Installation using Composer

You can install this extension via composer:

- Add the repository to your projects' composer.json:

```json
    {
        "repositories": [
            { "type": "vcs", "url":  "https://bitbucket.org/plus-b/pb_downloadform.git" }
        ]
    }
```

- Add the dependency to your project

```bash
    composer require plusb/pb_downloadform
```

Tested with Typo3 8.7.4

### Installation as extension from TYPO3 Extension Repository (TER)

Download and install the extension with the extension manager module.

## Contribute

Do you want to contribute to this extension? Do not hesitate and fork the repository at [Bitbucket](https://bitbucket.org/plus-b/pb_downloadform)

## Troubleshooting

- You can create an [Issue](https://bitbucket.org/plus-b/pb_downloadform/issues) here: [https://bitbucket.org/plus-b/pb_downloadform/issues](https://bitbucket.org/plus-b/pb_downloadform/issues)
- You can ask [plus B](http://www.plusb.de/) for support here: [http://www.plusb.de/kontakt/](http://www.plusb.de/kontakt/)  

