<?php
namespace PlusB\PbDownloadform\TypeConverter;

/***************************************************************
 *  Copyright notice
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *  (c) 2019 Arend Maubach <am@plusb.de>, plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use TYPO3\CMS\Extbase\Property\TypeConverter\AbstractTypeConverter;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Converter which transforms the hashedIdentifier of a FileReference to \TYPO3\CMS\Extbase\Domain\Model\FileReference.
 *
 */
class StorageUidAndHashToFileReferenceConverter extends AbstractTypeConverter {

	/**
	 * @var array<string>
	 */
	protected $sourceTypes = ['string'];

	/**
	 * @var string
	 */
	protected $targetType = 'TYPO3\\CMS\\Core\\Resource\\File';


    /**
     * @var \PlusB\PbDownloadform\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository = NULL;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     * @inject
     */
    protected $resourceFactory = NULL;

    /**
     * fileIndexRepository
     *
     * @var \TYPO3\CMS\Core\Resource\Index\FileIndexRepository
     * @inject
     */
    protected $fileIndexRepository = NULL;



    /**
     * Actually convert from $source to $targetType, taking into account the fully
     * built $convertedChildProperties and $configuration.
     *
     * The return value can be one of three types:
     * - an arbitrary object, or a simple type (which has been created while mapping).
     *   This is the normal case.
     * - NULL, indicating that this object should *not* be mapped (i.e. a "File Upload" Converter could return NULL if no file has been uploaded, and a silent failure should occur.
     * - An instance of \TYPO3\CMS\Extbase\Error\Error -- This will be a user-visible error message later on.
     * Furthermore, it should throw an Exception if an unexpected failure (like a security error) occurred or a configuration issue happened.
     *
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return mixed|\TYPO3\CMS\Extbase\Error\Error the target type, or an error object if a user-error occurred
     * @throws \TYPO3\CMS\Extbase\Property\Exception\TypeConverterException thrown in case a developer error occurred
     * @api
     */

    /**
     * @param mixed $source
     * @param string $targetType
     * @param array $convertedChildProperties
     * @param \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration
     * @return mixed|\TYPO3\CMS\Core\Resource\File|\TYPO3\CMS\Extbase\Error\Error
     */
    public function convertFrom($source, $targetType, array $convertedChildProperties = array(), \TYPO3\CMS\Extbase\Property\PropertyMappingConfigurationInterface $configuration = NULL) {
        $temp = explode(':', $source);
        $storageUid = (int) $temp[0];
        $identifierHash = $temp[1];
        $fileData = $this->fileIndexRepository->findOneByStorageUidAndIdentifierHash($storageUid, $identifierHash);
        $fileObject = $this->resourceFactory->getFileObject($fileData['uid']);
        return $fileObject;
    }
}
