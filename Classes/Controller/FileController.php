<?php

namespace PlusB\PbDownloadform\Controller;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *  (c) 2019 Arend Maubach <am@plusb.de>, plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use PlusB\PbDownloadform\Domain\Model\FileReference;
use PlusB\PbDownloadform\Domain\Model\UserData;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Mail\MailMessage;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\VersionNumberUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\GenericObjectValidator;


/**
 * FileController
 */
class FileController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * fileRepository
     *
     * @var \PlusB\PbDownloadform\Domain\Repository\FileRepository
     * @inject
     */
    protected $fileRepository = NULL;

    /**
     * userDataRepository
     *
     * @var \PlusB\PbDownloadform\Domain\Repository\UserDataRepository
     * @inject
     */
    protected $userDataRepository = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager = NULL;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\Mapper\DataMapper
     * @inject
     */
    protected $dataMapper;

    /**
     * The TypoScript Configuration Array
     *
     * @var array
     */
    protected $typoScript;

    /**
     * This is the key for config.baseURL
     *
     * @var string
     */
    const baseUrlKey = 'baseURL';

    /**
     * Returns the Base URI of the request
     *
     * @return mixed
     */
    private function initBaseUrlSetting()
    {
        if (empty($this->typoScript)) {
            $this->typoScript = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);
        }
        if (!array_key_exists(self::baseUrlKey, $this->settings) && array_key_exists('config.', $this->typoScript) && array_key_exists(self::baseUrlKey, $this->typoScript['config.'])) {
            $this->settings[self::baseUrlKey] = $this->typoScript['config.'][self::baseUrlKey];
        }
    }

    /**
     * when the setting 'disable-validation' is set, this function disables the Validation
     * for the properties of the userData-object.
     *
     * the validation rules are set via doc comments in the UserData class
     */
    private function initValidation()
    {
        $disableValidation = boolval($this->settings['disable-validation']);

        $validatorConjunctions = $this->validatorResolver->getBaseValidatorConjunction('PlusB\\PbDownloadform\\Domain\\Model\\UserData');
        $activeValidators = $validatorConjunctions->getValidators();

        //We first create a custom object validator, which will be added to the validatorConjunctions later on
        $CustomGenericObjectValidator = new GenericObjectValidator();
        if (VersionNumberUtility::convertVersionNumberToInteger(TYPO3_branch) < 7000000) {
            $CustomGenericObjectValidator->injectConfigurationManager($this->configurationManager);
        }

        //statically configure the settings and property names
        $formPropertiesConfig = [
            'nameEnabled' => 'name',
            'companyEnabled' => 'company',
            'telephoneEnabled' => 'telephone',
            'emailEnabled' => 'email',
            'zipEnabled' => 'zip',
            'cityEnabled' => 'city',
        ];

        //copy the current validators to our new custom validator
        foreach($activeValidators as $validator){
            foreach($formPropertiesConfig as $settingsName => $propertyName){
                $enableFormProperty = boolval($this->settings[$settingsName]);
                if($enableFormProperty){
                    foreach($validator->getPropertyValidators($propertyName) as $prop_validator){
                        $CustomGenericObjectValidator->addPropertyValidator($propertyName, $prop_validator);
                    }
                }
            }
        }

        //The trick here: we always remove the validation which was created automagically via the doc comments in the model class.
        //after that, we only add the needed validators of the enabled properties
        foreach($validatorConjunctions->getValidators() as $validator){
            $validatorConjunctions->removeValidator($validator);
        }

        if(!$disableValidation){
            $validatorConjunctions->addValidator($CustomGenericObjectValidator);
        }
    }

    /**
     * Creates a fileReference-row in the table sys_file_reference for the given fileReference-Data
     *
     * @param $fileReferenceData
     */
    private function createFileReferenceDataRow($fileReferenceData)
    {


        $connectionPool = GeneralUtility::makeInstance(ConnectionPool::class);
        $databaseConnectionForSysFileReference = $connectionPool->getConnectionForTable('sys_file_reference');
        $databaseConnectionForSysFileReference->insert(
            'sys_file_reference',
            $fileReferenceData
        );
        $databaseConnectionForSomeTable = $connectionPool->getConnectionForTable($fileReferenceData['tablenames']);
        $databaseConnectionForSomeTable->update(
            $fileReferenceData['tablenames'],
            ['file' => 1],
            ['uid' => $fileReferenceData['uid_foreign']]
        );
    }

    /**
     *
     * Sends the given file to the user and exits
     *
     * @param File $file
     */
    private function sendFile(File $file)
    {

        /**
         * TODO:
         *
         * This should to be done in a TYPO3 Extbase way like below, but there
         * is something wrong with this method.
         * Right now, we are satisfied with a pure PHP solution which does the job, but I am very interested
         * on implementing it with a Response-object in an extbase style manner.
         *
         *
         * $this->response->setHeader('Content-Description', 'File Transfer');
         * $this->response->setHeader('Content-Type', $file->getMimeType());
         * $this->response->setHeader('Content-Disposition', 'attachment; filename=' . $file->getName());
         * $this->response->setHeader('Expires', '0');
         * $this->response->setHeader('Cache-Control', 'must-revalidate');
         * $this->response->setHeader('Pragma', 'public');
         * $this->response->setHeader('Content-Length', $file->getSize());
         * $this->response->setContent($file->getContents());
         * $this->response->sendHeaders();
         * $this->response->send();
         *
         */
        header('Content-Description: File Transfer');
        header('Content-Type: ' . $file->getMimeType());
        header('Content-Disposition: attachment; filename=' . $file->getName());
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . $file->getSize());

        ob_clean();
        flush();
        echo $file->getContents();
        //ATTENTION: This exit is very important when you want to use the plugin more than one time per page,
        //because for every instance of the plugin, this action will be executed
        //and therefore multiple userData objects will be created
        exit;
    }

    /**
     * If the configuration is properly set, the plugin sends an email after every download
     *
     */
    private function sendEmailToAdmin(UserData $userData)
    {
        if (!empty($this->settings['email']['admin-address']) && (bool)$this->settings['email']['send']) {

            $configuration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
            $templateRootpath = GeneralUtility::getFileAbsFileName($configuration['view']['templateRootPath']);
            $extensionName = $this->request->getControllerExtensionName();
            $typo3MailConfiguration = $GLOBALS['TYPO3_CONF_VARS']['MAIL'];

            /** @var \TYPO3\CMS\Fluid\View\StandaloneView $emailTemplate */
            $emailTemplate = $this->objectManager->get(\TYPO3\CMS\Fluid\View\StandaloneView::class);
            $emailTemplate->setTemplatePathAndFilename($templateRootpath . $this->settings['email']['email-template']);
            $emailTemplate->assign('extensionName', $extensionName);
            $emailTemplate->assign('userData', $userData);
            $body = $emailTemplate->render();
            $subject = LocalizationUtility::translate('tx_pbdownloadform_domain_model_userdata.email.subject', $extensionName);
            $contentType = 'text/' . $this->settings['email']['content-type'];
            $charset = 'utf-8';

            $fromAddress = (!empty($this->settings['email']['from-address'])) ? $this->settings['email']['from-address'] : $typo3MailConfiguration['defaultMailFromAddress'];
            $fromName = (!empty($this->settings['email']['from-name'])) ? $this->settings['email']['from-name'] : $typo3MailConfiguration['defaultMailFromName'];

            $toAddress = (!empty($this->settings['email']['admin-address'])) ? $this->settings['email']['admin-address'] : $typo3MailConfiguration['defaultMailFromAddress'];
            $toName = (!empty($this->settings['email']['admin-name'])) ? $this->settings['email']['admin-name'] : $typo3MailConfiguration['defaultMailFromName'];

            $mail = new MailMessage();
            $mail->setSubject($subject);
            $mail->setBody($body, $contentType, $charset);
            $mail->setFrom(array($fromAddress => $fromName));
            $mail->setTo(array($toAddress => $toName));
            $mail->send();
        }
    }

    /**
     * Returns the current uid of the tt_content-element where the Plugin was inserted
     *
     * @return mixed
     */
    protected function getPageUid()
    {
        $cObj = $this->configurationManager->getContentObject();
        return $cObj->data['uid'];
    }

    /**
     *
     * Initializes the object
     *
     */
    public function initializeObject()
    {
        $this->initBaseUrlSetting();
        $this->initValidation();
    }

    /**
     * Initalizes all actions
     *
     */
    public function initializeAction()
    {

    }

    /**
     * initializes download action
     *
     * @return void
     */
    public function initializeDownloadAction()
    {
        $propertyMappingConfiguration = $this->arguments['userData']->getPropertyMappingConfiguration();
        $propertyMappingConfiguration->allowAllProperties();
        $propertyMappingConfiguration->setMapping('file', 'originalResource');
        $propertyMappingConfiguration->forProperty('originalResource')->setTypeConverter($this->objectManager->get(\PlusB\PbDownloadform\TypeConverter\StorageUidAndHashToFileReferenceConverter::class));

    }

    /**
     * list all selected files from the flexform plugin
     *
     * @return void
     */
    public function listAction()
    {
        $pageUid = $this->getPageUid();
        $files = $this->fileRepository->findByRelation('tt_content', 'download_form', $pageUid);
        $originalRequest = $this->request->getOriginalRequest();
        $userData = new UserData();

        //When there is a validation error, we want to pass the storageUidAndHash of the file into the form again
        if ($originalRequest !== NULL) {

            $oldUserData = $originalRequest->getArgument('userData');

            /** @var FileReference $file */
            foreach ($files as $file) {
                if ($oldUserData['file'] === $file->getOriginalFile()->getStorage()->getUid() . ':' . $file->getOriginalFile()->getHashedIdentifier()) {
                    $oldUserData['file'] = $file;
                }
            }
            $userData->initByArray($oldUserData);

            $this->view->assign('storageUidAndHash', $oldUserData['file']);
        }

        $this->view->assign('files', $files);
        $this->view->assign('userData', $userData);
        $this->view->assign('lightbox', $this->settings['popup']['lightbox']);
    }

    /**
     * After the user provides the userData, his data is persisted to the backend and the file can be downloaded
     * optionally, an Email to an Admin is sent
     *
     * TODO:
     * - persist the fileReference object in a good and clean way
     * - deliver the file in an extbase style way
     *
     * @param UserData $userData
     */
    public function downloadAction(\PlusB\PbDownloadform\Domain\Model\UserData $userData = NULL)
    {
        if ($userData === NULL) {
            $this->redirect('list');
        }

        $userData->updateFilepath();

        $this->userDataRepository->add($userData);
        $this->persistenceManager->persistAll();

        /**
         * TODO: Find a clean way to insert
         *
         * So, we tried many ways, but we did not find another clean extbase-style way
         * to do this except of doing it completely manually.
         *
         * I know, this is bad code-smell, but it seems that either the extbase Repository is not ready for FAL
         * or I did not find the right documentation. We needed a working solution, so I did it this way
         */
        $userDataTableName = $this->dataMapper->getDataMap(get_class($userData))->getTableName();
        $fieldname = 'file';

        $fileReferenceData = array(
            'uid_local' => $userData->getOriginalResource()->getUid(),
            'uid_foreign' => $userData->getUid(),
            'tablenames' => $userDataTableName,
            'fieldname' => $fieldname,
            'pid' => $this->userDataRepository->getStoragePid(),
            'table_local' => 'sys_file',
            'title' => $userData->getOriginalResource()->getName()
        );
        $this->createFileReferenceDataRow($fileReferenceData);  //TODO: Name it 'saveUserData' or something

        //Send the Email
        $this->sendEmailToAdmin($userData);

        //Send the chosen file
        $this->sendFile($userData->getOriginalResource());
    }

}