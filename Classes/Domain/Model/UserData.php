<?php
namespace PlusB\PbDownloadform\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
use PlusB\PbDownloadform\Domain\Validator\NameValidator;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * UserData
 */
class UserData extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity {

	/**
	 * Name
	 *
	 * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 2, maximum = 50, propertyName = name)
	 */
	protected $name = '';

	/**
	 * Company
	 *
	 * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 2, maximum = 50, propertyName = company)
	 */
	protected $company = '';

	/**
	 * Telephone Number
     *
     * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\AllowedCharactersValidator(allowedCharacters = "1,2,3,4,5,6,7,8,9,0,+,/,(,),-", propertyName = telephone, patternExample = "+491234-5678910")
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 4, maximum = 50, propertyName = telephone)
	 */
	protected $telephone = '';

	/**
	 * Email
	 *
	 * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\EmailAddressValidator
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 3, maximum = 50, propertyName = email)
	 */
	protected $email = '';

	/**
	 * Zip code
	 *
	 * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\AllowedCharactersValidator(allowedCharacters = "1,2,3,4,5,6,7,8,9,0", propertyName = zip, patternExample = "12345")
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 3, maximum = 5, propertyName = zip)
	 */
	protected $zip = '';

	/**
	 * City
	 *
	 * @var string
     * @validate \PlusB\PbDownloadform\Domain\Validator\PropertyValidator(minimum = 3, maximum = 50, propertyName = city)
	 */
	protected $city = '';

	/**
	 * file
	 *
	 * @var \PlusB\PbDownloadform\Domain\Model\FileReference
     *
	 */
	protected $file = NULL;

    /**
     * Was the checkbox for setting the cookie set?
     *
     * @var bool
     */
    protected $setCookie = FALSE;

    /**
     * We need a pointer to the File-object
     * to store a new FileReference
     *
     *
     * @var \TYPO3\CMS\Core\Resource\File
     *
     */
    protected $originalResource;

    /**
     * the path to the file for saving in the db
     *
     * @var string
     */
    protected $filepath;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName() {
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 * @return void
	 */
	public function setName($name) {
		$this->name = $name;
	}

	/**
	 * Returns the company
	 *
	 * @return string $company
	 */
	public function getCompany() {
		return $this->company;
	}

	/**
	 * Sets the company
	 *
	 * @param string $company
	 * @return void
	 */
	public function setCompany($company) {
		$this->company = $company;
	}

	/**
	 * Returns the telephone
	 *
	 * @return string $telephone
	 */
	public function getTelephone() {
		return $this->telephone;
	}

	/**
	 * Sets the telephone
	 *
	 * @param string $telephone
	 * @return void
	 */
	public function setTelephone($telephone) {
		$this->telephone = $telephone;
	}

	/**
	 * Returns the email
	 *
	 * @return string $email
	 */
	public function getEmail() {
		return $this->email;
	}

	/**
	 * Sets the email
	 *
	 * @param string $email
	 * @return void
	 */
	public function setEmail($email) {
		$this->email = $email;
	}

	/**
	 * Returns the zip
	 *
	 * @return string $zip
	 */
	public function getZip() {
		return $this->zip;
	}

	/**
	 * Sets the zip
	 *
	 * @param string $zip
	 * @return void
	 */
	public function setZip($zip) {
		$this->zip = $zip;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity() {
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 * @return void
	 */
	public function setCity($city) {
		$this->city = $city;
	}

	/**
	 * Returns the file
	 *
	 * @return \PlusB\PbDownloadform\Domain\Model\FileReference $file
	 */
	public function getFile() {
		return $this->file;
	}

	/**
	 * Sets the file
	 *
	 * @param \TYPO3\CMS\Core\Resource\FileInterface $file
	 * @return void
	 */
	public function setFile(\TYPO3\CMS\Core\Resource\FileInterface $file) {
		$this->file = $file;
	}

    /**
     * @return \TYPO3\CMS\Core\Resource\File
     */
    public function getOriginalResource() {
        return $this->originalResource;
    }

    /**
     * @param \TYPO3\CMS\Core\Resource\File $originalResource
     */
    public function setOriginalResource(\TYPO3\CMS\Core\Resource\File $originalResource) {
        $this->originalResource = $originalResource;
    }

    /**
     * @return boolean
     */
    public function getSetCookie(){
        return $this->setCookie;
    }

    /**
     * @param boolean $setCookie
     */
    public function setSetCookie($setCookie) {
        $this->setCookie = $setCookie;
    }

    /**
     * Returns the boolean state of setCookie
     *
     * @return boolean
     */
    public function isSetCookie() {
        return $this->setCookie;
    }

    /**
     * @return string
     */
    public function getFilepath() {
        return $this->filepath;
    }

    /**
     * @param string $filepath
     */
    public function setFilepath($filepath) {
        $this->filepath = $filepath;
    }

    /**
     *
     * Updates the property filepath
     *
     */
    public function updateFilepath(){
        if(!empty($this->originalResource)){
            $this->setFilepath($this->getOriginalResource()->getIdentifier());
        }
    }

    /**
     * @param array $userDataArray
     */
    public function initByArray($userDataArray)
    {
        $this->_setProperty('name', $userDataArray['name']);
        $this->_setProperty('company', $userDataArray['company']);
        $this->_setProperty('telephone', $userDataArray['telephone']);
        $this->_setProperty('email', $userDataArray['email']);
        $this->_setProperty('zip', $userDataArray['zip']);
        $this->_setProperty('city', $userDataArray['city']);
        $this->_setProperty('setCookie', boolval($userDataArray['setCookie']));

        $this->_setProperty('file', $userDataArray['file']);
    }

}