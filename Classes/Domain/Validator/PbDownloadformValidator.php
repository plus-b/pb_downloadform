<?php

namespace PlusB\PbDownloadform\Domain\Validator;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;
use TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



abstract class PbDownloadformValidator extends AbstractValidator {

    protected $acceptsEmptyValues = FALSE;

    protected $extensionName = 'pb_downloadform';

    protected $localisationKeyPrefix = 'tx_pbdownloadform_domain_model_userdata';

}