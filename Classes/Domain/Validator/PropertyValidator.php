<?php

namespace PlusB\PbDownloadform\Domain\Validator;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator;
use TYPO3\CMS\Extbase\Validation\Validator\StringLengthValidator;


/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *  (c) 2019 Arend Maubach <am@plusb.de>, plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/



class PropertyValidator extends PbDownloadformValidator {


    /**
     * @var array
     */
    protected $supportedOptions = [
        'minimum' => array(0, 'Minimum length for a valid string', 'integer'),
        'maximum' => array(PHP_INT_MAX, 'Maximum length for a valid string', 'integer'),
        'propertyName' => array('propertyName', 'The Name of the property', 'string'),
    ];

    /**
     * @param mixed $value
     * @throws \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException
     */
    public function isValid($value) {

        $propertyName = strtolower($this->options['propertyName']);

        if ($this->options['maximum'] < $this->options['minimum']) {
            throw new \TYPO3\CMS\Extbase\Validation\Exception\InvalidValidationOptionsException('The \'maximum\' is shorter than the \'minimum\' in the StringLengthValidator.', 1412586338);
        }

        if (is_object($value)) {
            if (!method_exists($value, '__toString')) {
                $this->addError('The given object could not be converted to a string.', 1412586339);
                return;
            }
        } elseif (!is_string($value)) {
            $this->addError('The given value was not a valid string.', 1412586340);
            return;
        }

        if($this->isEmpty($value)){
            $this->addError(
                $this->translateErrorMessage(
                    $this->localisationKeyPrefix . '.' . $propertyName . '.validation.empty',
                    $this->extensionName,
                    array()
                ), 1412586343, array($this->options['propertyName']));
        }else{

            // TODO Use \TYPO3\CMS\Core\Charset\CharsetConverter::strlen() instead; How do we get the charset?
            $stringLength = strlen($value);
            $isValid = TRUE;
            if ($stringLength < $this->options['minimum']) {
                $isValid = FALSE;
            }
            if ($stringLength > $this->options['maximum']) {
                $isValid = FALSE;
            }

            if ($isValid === FALSE) {
                if ($this->options['minimum'] > 0 && $this->options['maximum'] < PHP_INT_MAX) {
                    $this->addError(
                        $this->translateErrorMessage(
                            $this->localisationKeyPrefix . '.' . $propertyName . '.validation.between',
                            $this->extensionName,
                            array (
                                $this->options['minimum'],
                                $this->options['maximum']
                            )
                        ), 1412586341, array($this->options['propertyName'], $this->options['minimum'], $this->options['maximum']));
                } elseif ($this->options['minimum'] > 0) {
                    $this->addError(
                        $this->translateErrorMessage(
                            $this->localisationKeyPrefix . '.' . $propertyName . '.validation.less',
                            $this->extensionName,
                            array(
                                $this->options['minimum']
                            )
                        ), 1412586342, array($this->options['propertyName'], $this->options['minimum']));
                } else {
                    $this->addError(
                        $this->translateErrorMessage(
                            $this->localisationKeyPrefix . '.' . $propertyName . '.validation.exceed',
                            $this->extensionName,
                            array(
                                $this->options['maximum']
                            )
                        ), 1412586343, array($this->options['propertyName'], $this->options['maximum']));
                }
            }
        }
    }
}