<?php
namespace PlusB\PbDownloadform\ViewHelpers;

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3Fluid\Fluid\Core\ViewHelper\Traits\CompileWithRenderStatic;

/***************************************************************
 *  Copyright notice
 *  (c) 2014 Samir Rachidi <sr@plusb.de>, Plus B
 *  (c) 2019 Arend Maubach <am@plusb.de>, plus B
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/
/**
 * Returns two classes by a given mimeType:
 *
 * <mediaType> <subType>
 *
 * e.g. mimeType is 'application/pdf' it will return the string 'application pdf'
 *
 * @param string $mimeType
 * @return string
 */
class MimeTypeToClassViewHelper extends AbstractViewHelper {

    /**
     * @return void
     */
    public function initializeArguments() {
        $this->registerArgument('mimeType', 'string', 'mime type of file {pb:mimeTypeToClass(mimeType: file.mimeType)}');
    }

    use CompileWithRenderStatic;



    public function render() {
        $mimeType = $this->arguments['mimeType'];

        if($mimeType === NULL){
            return '';
        }
        return preg_replace('/\//', ' ', $mimeType);
    }
}
