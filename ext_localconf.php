<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
	'PlusB.' . 'pb_downloadform',
	'Downloadform',
	array(
		'File' => 'list, download',
		
	),
	// non-cacheable actions
	array(
        'File' => 'download',
	)
);


call_user_func(
    function()
    {
        // wizards
        if (TYPO3_MODE === 'BE') {
        //include page ts config globally for the whole cms system
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
                '<INCLUDE_TYPOSCRIPT: source="FILE:EXT:pb_downloadform/Configuration/PageTSconfig/Page.tsconfig">'
            );
        }

        // > 8.7 Register icon of tt_content element of CType="pbdownloadform_downloadform" in page module
        $iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
        $iconRegistry->registerIcon(
            'pbdownloadform_downloadform',
            \TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
            ['source' => 'EXT:pb_downloadform/Resources/Public/Icons/downloadform.svg']
        );
    }
);
