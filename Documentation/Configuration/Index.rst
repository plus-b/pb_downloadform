﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _configuration:

Configuration Reference
=======================

Dear Typo3 Integrator,

the next steps will hopefully explain you how to use this extension.

1. Go to the extension manager, find this extension and install.
2. Include the extension typoscript
3. Navigate to an empty page and insert the "Download Form"-Plugin
4. Open the flexform and add some files
5. Navigate to the "Field Settings" tab and activate the field you want to display.
6. Insert the correct Email-addresses in the typoscript constants (Admin Email Address, Admin From Email Address)
7. clear all caches and enjoy the result.


.. _configuration-typoscript:

TypoScript Reference
--------------------

You can use the constant editor to edit some basic behaviors:

- Default storage PID[plugin.tx_pbdownloadform.persistence.storagePid]
 - Insert the page ID, where the form data should be saved
- Disable Validation?[plugin.tx_pbdownloadform.settings.disable-validation]
 - If set all inserted form data will not be validated (default: false)
- Admin Email Template[plugin.tx_pbdownloadform.settings.email.email-template]
 - Insert an own Email template
- Load the default CSS code of the Extension[plugin.tx_pbdownloadform.settings.load-default-css]
 - If you want to use your own CSS, turn this option off (default: false)
- Admin From Email Name[plugin.tx_pbdownloadform.settings.email.from-name]
 - Set the Name emails are sent from
- Admin From Email Address[plugin.tx_pbdownloadform.settings.email.from-address]
 - Set the Email Address emails are sent from
- Load jQuery?[plugin.tx_pbdownloadform.settings.load-jquery]
 - Include jQuery? (default: false)
- Admin Email Address[plugin.tx_pbdownloadform.settings.email.admin-address]
 - Set the Email Address emails are sent to
- Admin Email Name[plugin.tx_pbdownloadform.settings.email.admin-name]
 - Set the Name emails are sent to
- Content Type ('html' or 'plain')[plugin.tx_pbdownloadform.settings.email.content-type]
 - Set the Content Type of emails (default: plain)
- Send Admin Email[plugin.tx_pbdownloadform.settings.email.send]
 - Enable/disable sending of mails (default: true)


.. _configuration-faq:

FAQ
---

Nothing or only fragments visible ?
=> check if static template is included
=> check typo3 log (/typo3temp/log/log.txt)

Click on file do not open popup ?
=> check if javascript is enabled
=> check if jQuery is loaded
=> check if AdBlock is running

.. _contribute:

Contribute
----------

Do you want to contribute to this extension? Do not hesitate and fork the repository at https://bitbucket.org/plus-b/pb_downloadform/
