﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _introduction:

Introduction
============

With this extension we decided to fill a gap of frontend download controlling through the use of a frontend-form that
need to be to be filled with user data to get a download. Our Goal is it to provide an easy and time-saving way of
managing frontend downloads. As you know nothing is perfect but we will give our best to make this extension as
comfortable as you need it. For this we need your Feedback, so if you need something or have something to say,
don't hesitate to contact us. Either write us an email at info@plusb.de.


.. _what-it-does:

What does it do?
----------------

This extension provides a simple plugin which displays a form which must be filled before a user can download a file. The files can be selected via FAL.
After the user filled the form and downloads the file, a database record containing the user input will be created. Also an email will be send
to the given e-mail address. There are several configuration parameters to control the behaviour of the plugin.
