﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: Includes.txt

.. _start:

=============================================================
plus B Download Form with FAL
=============================================================

.. only:: html

	:Classification:
		pb_downloadform

	:Version:
		|release|

	:Language:
		en

	:Description:
		This extension provides a simple plugin displaying a form which must be filled before a user can download a file. The files can be selected via FAL to any page element.

	:Keywords:
		plusb,downloadform

	:Copyright:
		2014-2017

	:Author:
		Samir Rachidi, plus B

	:Email:
		sr@plusb.de

	:License:
		This document is published under the Open Content License
		available from http://www.opencontent.org/opl.shtml

	:Rendered:
		|today|

	The content of this document is related to TYPO3,
	a GNU/GPL CMS/Framework available from `www.typo3.org <http://www.typo3.org/>`_.

	**Table of Contents**

.. toctree::
	:maxdepth: 5
	:titlesonly:
	:glob:

	Introduction/Index
	Configuration/Index
