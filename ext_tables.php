<?php
if (!defined('TYPO3_MODE')) {
	die('Access denied.');
}

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'pb_downloadform',
	'Downloadform',
	'Download Form'
);

//Add the flexform configuration for the plugin
$pluginSignature = str_replace('_', '', 'pb_downloadform') . '_downloadform';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:' . 'pb_downloadform' . '/Configuration/FlexForms/flexform_downloadform.xml');

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('pb_downloadform', 'Configuration/TypoScript', 'plus B Download Form with FAL');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_pbdownloadform_domain_model_userdata', 'EXT:pb_downloadform/Resources/Private/Language/locallang_csh_tx_pbdownloadform_domain_model_userdata.xlf');
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_pbdownloadform_domain_model_userdata');