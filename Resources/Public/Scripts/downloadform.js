/**
 * Powered by jQuery-Boilerplate
 *
 * http://jqueryboilerplate.com/
 *
 */

// the semi-colon before function invocation is a safety net against concatenated
// scripts and/or other plugins which may not be closed properly.
;(function ($, window, document, undefined) {

    // undefined is used here as the undefined global variable in ECMAScript 3 is
    // mutable (ie. it can be changed by someone else). undefined isn't really being
    // passed in so we can ensure the value of it is truly undefined. In ES5, undefined
    // can no longer be modified.

    // window and document are passed through as local variable rather than global
    // as this (slightly) quickens the resolution process and can be more efficiently
    // minified (especially when both are regularly referenced in your plugin).

    // Create the defaults once
    var pluginName = "Downloadform",
        defaults = {
            propertyName: "value"
        };

    // The actual plugin constructor
    function Plugin(element, options) {

        var self = this;
        this.fileLinkSelector = 'a.file[href="#"]';
        this.element = $(element);

        this.form = this.element.find('form.js-downloadform');
        this.file = this.form.find('input[name*="file"]');
        this.fileList = this.element.find(this.fileLinkSelector);
        this.fileHash = this.form.find('input[name*="file"]').val();
        this.userData = null;
        this.lightbox = null;
        this.cookieName = 'pb_downloadform';
        this.downloadedCookie = this.cookieName + '_downloaded';

        // jQuery has an extend method which merges the contents of two or
        // more objects, storing the result in the first object. The first object
        // is generally empty as we don't want to alter the default options for
        // future instances of the plugin
        this.settings = $.extend({}, defaults, options);
        this._defaults = defaults;
        this._name = pluginName;

        this.init();
    }

    // Avoid Plugin.prototype conflicts
    $.extend(Plugin.prototype, {
        init: function () {
            // Place initialization logic here
            // You already have access to the DOM element and
            // the options via the instance, e.g. this.element
            // and this.settings
            // you can add more functions like the one below and
            // call them like so: this.yourOtherFunction(this.element, this.settings).

            var self = this;
            $(this.element).find(this.fileLinkSelector).click(function (e) {
                e.preventDefault();
                self.addFileHashToForm($(this).data('hash'));
            });

            //Submit binding for form
            $(this.form).submit(function (e) {
                if (!self.formHasFile()) {
                    window.alert(self.element.data('error-no-file'));
                    e.preventDefault();
                }

                self.saveCookie();
                self.closeLightbox();
            });

            this.initLightbox();

            if (this.formHasError()) {
                this.fileList.filter('[data-hash="' + this.fileHash + '"]').click();
            }

            this.initUserdata();
        },

        toggleErrorMessages: function (show) {
            if (show) {
                this.form.find('.downloadform__message--error').css('display', 'block');
            } else {
                this.form.find('.downloadform__message--error').css('display', 'none');
            }
        },

        /**
         * When the user checks the 'setCookie' checkbox, the form data will be loaded
         * from the cookie, if available and only, if no validation errors happened
         */
        initUserdata: function () {

            this.initUserDataByForm();

            var cookieData = this.getCookie(this.cookieName);
            if (!this.formHasError() && cookieData !== '') {

                this.setUserData($.parseJSON(cookieData));
                this.initFormByUserData();
            }

        },


        addFileHashToForm: function (fileHash) {
            this.file.val(fileHash);
        },

        formHasFile: function () {
            return (this.file.val() !== '' && this.file.val().indexOf(':') !== -1);
        },

        formHasError: function () {
            return (this.form.find('.downloadform__message--error').length > 0);
        },

        initLightbox: function () {
            var self = this;
            this.fileList.magnificPopup({
                midClick: true,
                mainClass: 'mfp-fade',
                removalDelay: 250,
                items: {
                    type: 'inline',
                    src: self.form.closest('.downloadform')
                    //closeBtnInside: false
                },
                callbacks: {
                    open: function () {
                        self.initUserdata();
                    },
                    close: function () {
                        self.saveCookie();
                    }
                }
            });
            self.lightbox = $.magnificPopup.instance;

        },

        initUserDataByForm: function () {

            var setCookieCheckbox = this.form.find('input[type="checkbox"][name*="setCookie"]');
            var setCookie = setCookieCheckbox.prop('checked');

            var userData = {
                name: this.form.find('input[name*="name"]').val(),
                company: this.form.find('input[name*="company"]').val(),
                telephone: this.form.find('input[name*="telephone"]').val(),
                email: this.form.find('input[name*="email"]').val(),
                zip: this.form.find('input[name*="zip"]').val(),
                city: this.form.find('input[name*="city"]').val(),
                setCookie: setCookie,
            };

            this.setUserData(userData);
        },

        getUserData: function () {
            return this.userData;
        },

        setUserData: function (userData) {
            this.userData = userData;
        },

        initFormByUserData: function () {

            var userData = this.getUserData();

            this.form.find('input[name*="name"]').val((userData.name));
            this.form.find('input[name*="company"]').val(userData.company);
            this.form.find('input[name*="telephone"]').val(userData.telephone);
            this.form.find('input[name*="email"]').val(userData.email);
            this.form.find('input[name*="zip"]').val(userData.zip);
            this.form.find('input[name*="city"]').val(userData.city);
            this.form.find('input[type="checkbox"][name*="setCookie"]').prop('checked', (userData.setCookie) ? 'checked' : '');
        },

        isSetCookie: function () {
            var userData = this.getUserData();
            return userData.setCookie;
        },

        saveCookie: function () {

            var self = this;
            var date = new Date();
            date.setTime(date.getTime() + 365 * 24 * 60 * 60 * 1000);
            self.initUserDataByForm();

            var userData = self.getUserData();
            if (self.isSetCookie()) {
                document.cookie = this.cookieName + '=' + JSON.stringify(userData) + '; path=/; expires=' + date.toGMTString() + ';';
            } else {
                document.cookie = this.cookieName + '=' + '' + '; path=/; expires=' + date.toGMTString() + ';';
            }
        },

        /**
         * This method returns a cookie by a given cookie Name
         *
         * source: http://www.w3schools.com/js/js_cookies.asp
         *
         */
        getCookie: function (cookieName) {
            var name = cookieName + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') c = c.substring(1);
                if (c.indexOf(name) != -1) return c.substring(name.length, c.length);
            }
            return "";
        },

        closeLightbox: function () {
            if (this.lightbox !== null) {
                this.lightbox.close();
            }
        }
    });

    // A really lightweight plugin wrapper around the constructor,
    // preventing against multiple instantiations
    $.fn[pluginName] = function (options) {
        this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin(this, options));
            }
        });

        // chain jQuery functions
        return this;
    };

})(jQuery, window, document);

$(function () {
    $('.tx-pb-downloadform').Downloadform();
});